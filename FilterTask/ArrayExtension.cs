﻿using System;
using System.Collections.Generic;

namespace FilterTask
{
    public static class ArrayExtension
    {
        public static int[] FilterByDigit(int[] source, int digit)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (source.Length == 0)
            {
                throw new ArgumentException("Source is empty.");
            }

            if (digit < 0 || digit > 9)
            {
                throw new ArgumentOutOfRangeException(nameof(digit));
            }

            List<int> containDigit = new List<int>();

            for (int i = 0; i < source.Length; i++)
            {
                if (IsContainDigit(source[i], digit))
                {
                    containDigit.Add(source[i]);
                }
            }

            return containDigit.ToArray(); 
        }

        public static bool IsContainDigit(int number, int digit)
        {
            number = Math.Abs(number);

            if (number == digit)
            {
                return true;
            }

            while (number != 0)
            {
                int sourceDigit = number % 10;
                if (sourceDigit == digit)
                {
                    return true;
                }

                number /= 10;
            }

            return false;
        }
    }
}
